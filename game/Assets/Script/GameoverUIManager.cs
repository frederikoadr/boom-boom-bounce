﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Proyecto26;

public class GameoverUIManager : MonoBehaviour
{
    [SerializeField] Transform podiumContainer, namePodiumContainer, otherContainer;
    [SerializeField] GameObject picturePodium, playerPodiumResult, playerResult;
    [SerializeField] RectTransform verticalLayoutCont;
    [SerializeField] Sprite[] flowers, medals, animals;
    void OnEnable()
    {
        List<int> playerRankSort;
        if (UIManager.instance.playerRankSort != null)
        {
            playerRankSort = UIManager.instance.playerRankSort;
            if(playerRankSort.Count > 0)
            {
                for (int i = 0; i < playerRankSort.Count; i++) 
                {
                    int _id = playerRankSort[playerRankSort.Count - i - 1]; //getting list
                    string ordinal = string.Empty;
                    if (i < 3)
                    {
                        if (i + 1 == 1)
                        {
                            ordinal = "st";
                        }
                        else if (i + 1 == 2)
                        {
                            ordinal = "nd";
                        }
                        else if (i + 1 == 3)
                        {
                            ordinal = "rd";
                        }
                        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
                        {
                            GameObject ipicturePodium = Instantiate(picturePodium, podiumContainer);
                            ipicturePodium.transform.GetChild(0).GetComponent<Image>().sprite = medals[i];
                            ipicturePodium.transform.GetChild(1).GetComponent<Image>().sprite = animals[_player.animalId]; //buat ganti animal
                            ipicturePodium.transform.GetChild(2).GetComponent<Image>().sprite = flowers[i];
                            GameObject namePodium = Instantiate(playerPodiumResult, namePodiumContainer);
                            namePodium.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _player.username;
                            namePodium.transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = (i+1).ToString() + ordinal;
                            if(i == 2)
                            {
                                ipicturePodium.transform.SetSiblingIndex(0);
                                namePodium.transform.SetSiblingIndex(0);
                            }
                            if (_player.username != "Guest")
                            {
                                RestClient.Get<UserData>("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + _player.username + ".json").Then((System.Action<UserData>)(response =>
                                {
                                    if(response.username == _player.username)
                                    {
                                        Debug.Log("winner valid " + response.username);
                                        UserData userData = new UserData(_player.username, response.password, true);
                                        RestClient.Put("https://praktikum-jaringan-komputer-default-rtdb.asia-southeast1.firebasedatabase.app/" + _player.username + ".json", userData);
                                    }
                                }), response =>
                                {
                                    Debug.Log("Winner not exist!");
                                });
                            }
                        }
                        else
                        {
                            Debug.Log($"player {_id} does not exist");
                        }
                    }
                    else if(i >= 3)
                    {
                        ordinal = "th";
                        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
                        {
                            GameObject iplayerResult = Instantiate(playerResult, otherContainer);
                            iplayerResult.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = _player.username;
                            iplayerResult.transform.GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = (i + 1).ToString() + ordinal;
                        }
                        else
                        {
                            Debug.Log($"player {_id} does not exist");
                        }
                    }
                }
                StartCoroutine(animatingSeq(playerRankSort.Count));
                Client.instance.Disconnect();
            }
        }
    }
    IEnumerator animatingSeq(int total)
    {
        podiumContainer.transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        namePodiumContainer.transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(2);

        podiumContainer.transform.GetChild(podiumContainer.transform.childCount - 1).gameObject.SetActive(true); //get last
        yield return new WaitForSeconds(2);
        namePodiumContainer.transform.GetChild(podiumContainer.transform.childCount - 1).gameObject.SetActive(true);
        yield return new WaitForSeconds(2);


        if (total >= 3)
        {
            podiumContainer.transform.GetChild(1).gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            namePodiumContainer.transform.GetChild(1).gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
        }


        podiumContainer.GetComponent<HorizontalLayoutGroup>().childControlHeight = true;
        verticalLayoutCont.LeanSize(new Vector2(0, 0), 1).setEaseOutExpo().setOnComplete(ShowOthers).delay = 0.1f;
    }
    void ShowOthers()
    {
        otherContainer.gameObject.SetActive(true);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
