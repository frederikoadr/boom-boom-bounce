﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static Dictionary<int, PlayerManager> players = new Dictionary<int, PlayerManager>();
    public static Dictionary<int, ItemSpawner> itemSpawners = new Dictionary<int, ItemSpawner>();
    public static Dictionary<int, ProjectileManager> projectiles = new Dictionary<int, ProjectileManager>();
    public static Dictionary<int, EnemyManager> enemies = new Dictionary<int, EnemyManager>();

    public GameObject localPlayerPrefab;
    public GameObject playerPrefab;
    public GameObject itemSpawnerPrefab;
    public GameObject projectilePrefab;
    public GameObject enemyPrefab;
    public GameObject towerStart;
    public GameObject matrasStart;
    public GameObject batu;
    public GameObject bola;
    public GameObject tree;
    public GameObject jejak;
    public GameObject treeShadow;
    public static int totalPlayer;
    public static int playerDieCounter;
    public static int playerAlive;

    [SerializeField] GameObject cdPanel;

    public GameObject zona;

    public void ResetDictionary()
    {
        players.Clear();
        playerDieCounter = 0;
    }

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }

    }

    private void Update()
    {
        /*Debug.Log(playerDieCounter);*/
        totalPlayer = players.Count;
        playerAlive = totalPlayer - playerDieCounter;
        //playerDieCounter = 0;
        //Debug.Log(playerAlive);
        //playerDieCounter = 0;
    }

    /// <summary>Spawns a player.</summary>
    /// <param name="_id">The player's ID.</param>
    /// <param name="_name">The player's name.</param>
    /// <param name="_position">The player's starting position.</param>
    /// <param name="_rotation">The player's starting rotation.</param>
    public void SpawnPlayer(int _id, string _username, Vector3 _position, Quaternion _rotation, int _animalId)
    {
        GameObject _player;
        if (_id == Client.instance.myId)
        {
            _player = Instantiate(localPlayerPrefab, _position, _rotation);
        }
        else
        {
            _player = Instantiate(playerPrefab, _position, _rotation);
        }

        _player.GetComponent<PlayerManager>().Initialize(_id, _username, _animalId);
        players.Add(_id, _player.GetComponent<PlayerManager>());
        UIManager.instance.AddPlayerList(_id, _username);
    }
    public void StartCountdown(int time)
    {
        cdPanel.SetActive(true);
        TextMeshProUGUI timeText = cdPanel.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        timeText.text = time.ToString("0");
        if (time == 0)
        {
            jejak.SetActive(false);
            cdPanel.SetActive(false);
            towerStart.SetActive(false);
            matrasStart.SetActive(false);
            bola.SetActive(true);
            batu.SetActive(true);
            tree.SetActive(true);
            treeShadow.SetActive(true);
        }
    }



    public void StartZone(int time, Vector3 position, Vector3 size)
    {
        zona.GetComponent<DieZone>().setZoneSize(position, size);
        if(time >= 30 && time < 40 )
        {
            FindObjectOfType<Countdown>().startZoneCount();
        }


        if (time >= 110 && time < 120)
        {
            FindObjectOfType<Countdown>().startZoneCount();
        }

    }

    public void CreateItemSpawner(int _spawnerId, Vector3 _position, bool _hasItem)
    {
        GameObject _spawner = Instantiate(itemSpawnerPrefab, _position, itemSpawnerPrefab.transform.rotation);
        _spawner.GetComponent<ItemSpawner>().Initialize(_spawnerId, _hasItem);
        itemSpawners.Add(_spawnerId, _spawner.GetComponent<ItemSpawner>());
    }

    public void SpawnProjectile(int _id, Vector3 _position)
    {
        GameObject _projectile = Instantiate(projectilePrefab, _position, Quaternion.identity);
        _projectile.GetComponent<ProjectileManager>().Initialize(_id);
        projectiles.Add(_id, _projectile.GetComponent<ProjectileManager>());
    }

    public void SpawnEnemy(int _id, Vector3 _position)
    {
        GameObject _enemy = Instantiate(enemyPrefab, _position, Quaternion.identity);
        _enemy.GetComponent<EnemyManager>().Initialize(_id);
        enemies.Add(_id, _enemy.GetComponent<EnemyManager>());
    }
}