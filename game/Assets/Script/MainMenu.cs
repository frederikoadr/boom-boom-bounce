﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject dissClient;
    public void Play()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void OpenMenu()
    {
        Destroy(GameObject.Find("BackgroundAudio"));
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
    public void ReloadScene()
    {
        
        
        //disini mungkin ditambahi disconnecting gracefully ya sebelum reload scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
