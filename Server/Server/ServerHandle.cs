﻿using System;
using System.Numerics;
using System.Collections.Generic;
using System.Text;

namespace Server
{
    class ServerHandle
    {
        public static void WelcomeReceived(int _fromClient, Packet _packet)
        {
            int _clientIdCheck = _packet.ReadInt();
            string _password = _packet.ReadString();
            string _username = _packet.ReadString();
            
            Console.WriteLine("username = " + _username);
            Console.WriteLine("password = " + _password);
            Console.WriteLine($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {_fromClient}.");
            
            if (_fromClient != _clientIdCheck)
            {
                Console.WriteLine($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})!");
            }
            Server.clients[_fromClient].SendIntoGame(_username);
        }

        public static void PlayerMovement(int _fromClient, Packet _packet)
        {
            //bool[] _inputs = new bool[_packet.ReadInt()];
            //for (int i = 0; i < _inputs.Length; i++)
            //{
            //    _inputs[i] = _packet.ReadBool();
            //}
            //Quaternion _rotation = _packet.ReadQuaternion();
            //Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
            Vector3 vector3 = _packet.ReadVector3();
            Server.clients[_fromClient].player.SetPos(vector3);
        }
        public static void PlayerRotation(int fromClient, Packet packet)
        {
            Quaternion quaternion = packet.ReadQuaternion();
            Server.clients[fromClient].player.SetRot(quaternion);
        }
    }
}