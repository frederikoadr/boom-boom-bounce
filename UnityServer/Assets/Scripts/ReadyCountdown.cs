﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadyCountdown : MonoBehaviour
{
    bool allReady = false;
    int readyCount = 0;
    float currentTime = 0f;
    [SerializeField] float startTime = 5f;
    GameObject[] knock;
    public GameObject bola;
    public GameObject batu;
    public GameObject matras;
    public GameObject tower;
    public GameObject zona;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = startTime;
    }
    public void Reset()
    {
        Debug.Log("reset");
        allReady = false;
        readyCount = 0;
        currentTime = startTime;
        bola.SetActive(false);
        batu.SetActive(false);
        matras.SetActive(true);
        tower.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (allReady && currentTime >= 0)
        {
            currentTime -= 1 * Time.deltaTime;
            ServerSend.CountdownStart((int)currentTime);
            if((int)currentTime == 0)
            {
                zona.GetComponent<DieZone>().StartStopwatch();
                bola.SetActive(true);
                batu.SetActive(true);
                matras.SetActive(false);
                tower.SetActive(false);
                GameManager.gameStart = allReady;
                GameManager.playerAlive = Server.CurrentPlayers;
                allReady = false;
                knock = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject g in knock)
                {
                    g.GetComponent<knockback>().enabled = true;
                    g.transform.position = new Vector3(Random.Range(-12f, 12f), Random.Range(-1f, 9f));
                }
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!GameManager.gameStart && other.CompareTag("Player"))
        {
            readyCount++;
            int totalPlyer = GameObject.FindGameObjectsWithTag("Player").Length;
            if (readyCount > 1 && totalPlyer == readyCount)
            {
                Debug.Log("starting in 5...");
                allReady = true;
                GameManager.playerAlive = totalPlyer;
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            readyCount--;
        }
    }
}
